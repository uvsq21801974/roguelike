package uvsq.m1info.roguelike;

import uvsq.m1info.roguelike.agents.Creature;

/**
 * A level up option with its bonus
 *
 */
public abstract class LevelUpOption {
	/**
	 * Name of the bonus
	 */
	private String name;
	/**
	 * Name
	 * @return
	 */
	public String name() { return name; }
	
	/**
	 * Constructor
	 * @param name
	 */
	public LevelUpOption(String name){
		this.name = name;
	}
	
	/**
	 * apply the bonus
	 * @param creature
	 */
	public abstract void invoke(Creature creature);
}
