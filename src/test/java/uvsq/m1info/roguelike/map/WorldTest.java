package uvsq.m1info.roguelike.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uvsq.m1info.roguelike.StuffFactory;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;

public class WorldTest {

	private World world;
	private StuffFactory factory;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		world = new WorldBuilder(90, 32, 5).makeCaves().build();
		factory = new StuffFactory(world);
	}

	@Test
	public void testWidth() {
		assertTrue(world.width() == 90);
	}

	@Test
	public void testHeight() {
		assertTrue(world.height() == 32);
	}

	@Test
	public void testDepth() {
		assertTrue(world.depth() == 5);
	}

	@Test
	public void testWorld() {
		assertNotNull(world);
	}

	@Test
	public void testCreature() {
		Creature creature = factory.newCreature("Bat", 0);
		assertTrue(world.creature(creature.x, creature.y, creature.z) == creature);
	}

	@Test
	public void testTile() {
		assertTrue(world.tile(-1, 0, 0) == Tile.BOUNDS);
	}

	@Test
	public void testGlyph() {
		Creature creature = factory.newCreature("Bat", 0);
		assertTrue(world.glyph(creature.x, creature.y, creature.z) == creature.glyph());
	}

	@Test
	public void testColor() {
		Creature creature = factory.newCreature("Bat", 0);
		assertTrue(world.color(creature.x, creature.y, creature.z) == creature.color());
	}

	@Test
	public void testOpenChest() {
		Creature player = factory.newPlayer(null, null);
		int i=0;
		while (i++<10) {
			Tile t = Tile.CHEST;
			int x = (int) (Math.random() * world.width());
			int y = (int) (Math.random() * world.height());
			int z = (int) (Math.random() * world.depth());
			world.setTestTile(x, y, z, t);
			world.openChest(x, y, z, player);

		}
		for (Creature c : world.getCreatures()) {
			if (c.name().contains("mimic")) {
				player = c;
				break;
			}
		}

		assertNotNull(player);
	}

	@Test
	public void testMine() {
		Tile t = Tile.WALL2;

		int x = (int) (Math.random() * world.width());
		int y = (int) (Math.random() * world.height());
		int z = (int) (Math.random() * world.depth());
		world.setTestTile(x, y, z, t);
		world.mine(x, y, z, 1);
		assertTrue(world.tile(x, y, z).wallState()!=t.wallState());
		
	}

	@Test
	public void testDig() {
		Tile t = Tile.FLOOR;

		int x = (int) (Math.random() * world.width());
		int y = (int) (Math.random() * world.height());
		int z = (int) (Math.random() * world.depth());
		world.setTestTile(x, y, z, t);
		
		world.dig(x, y, z);
		assertTrue(world.tile(x, y, z).glyph()!=t.glyph());
	}

	@Test
	public void testAddAtEmptyLocationCreatureInt() {
		Creature player = factory.newPlayer(null, null);
		world.addAtEmptyLocation(player, 0);
		assertTrue(world.creature(player.x, player.y, player.z).name().contains("player"));
	}

	@Test
	public void testRemoveCreature() {
		Creature player = factory.newPlayer(null, null);
		assertTrue(world.creature(player.x, player.y, player.z).name().contains("player"));
		world.remove(player);
		assertFalse(world.getCreatures().contains(player));
	}

	@Test
	public void testRemoveItem() {
		Item item = StuffFactory.newFood();
		world.addAtEmptyLocation(item, 0);
		assertNotNull(world.item(item.getX(), item.getY(), item.getZ()));
		world.remove(item);
		assertNull(world.item(item.getX(), item.getY(), item.getZ()));
	}

	@Test
	public void testAddAtEmptyLocationItemInt() {
		Item item = StuffFactory.newFood();
		world.addAtEmptyLocation(item, 0);
		assertNotNull(world.item(item.getX(), item.getY(), item.getZ()));
	}

	@Test
	public void testRemoveIntIntInt() {
		Item item = StuffFactory.newFood();
		world.addAtEmptyLocation(item, 0);
		assertNotNull(world.item(item.getX(), item.getY(), item.getZ()));
		world.remove(item.getX(),item.getY(),item.getZ());
		assertNull(world.item(item.getX(), item.getY(), item.getZ()));
	}

	@Test
	public void testAddAtEmptySpace() {
		Item item = StuffFactory.newFood();
		int x = (int) (Math.random() * world.width());
		int y = (int) (Math.random() * world.height());
		int z = (int) (Math.random() * world.depth());
		world.setTestTile(x, y, z, Tile.FLOOR);
		world.addAtEmptySpace(item, x,y,z);
		assertNotNull(item.getLocation());
	}

	@Test
	public void testNeighboringPNJ() {
		int x = (int) (Math.random() * world.width());
		int y = (int) (Math.random() * world.height());
		int z = (int) (Math.random() * world.depth());
		assertNotNull(world.neighboringPNJ(x, y, z));
	}

	@Test
	public void testAdd() {
		Creature player = factory.newPlayer(null, null);
		world.add(player);
		assertTrue(world.creature(player.x, player.y, player.z).name().contains("player"));
	}

}
