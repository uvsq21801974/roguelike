package uvsq.m1info.roguelike;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import uvsq.m1info.roguelike.screens.PlayScreen;

public class Persistance{

	/**
	 * name of the directory to save the file in
	 */
	private static String  dirPath = "saves";
	/**
	 * adress of the saved game
	 */
	private static String name = "saves/save";
	
	/**
	 * Save the playscreen
	 * @param playScreen
	 * @throws IOException
	 */
	public static void save(PlayScreen playScreen) throws IOException {
		File dir = new File(dirPath);
		if(!dir.exists()) {
			dir.mkdirs();
			File fichier =  new File(name);
			try {
				fichier.createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			execute(fichier,playScreen);
		}else if (dir.exists()) {
			File fichier =  new File(name);
			if(fichier.exists()){
				fichier.delete();
				fichier.createNewFile();
				execute(fichier,playScreen);
			}else {
				fichier.createNewFile();
				execute(fichier,playScreen);
			}
		}			
	}

	/**
	 * load the game if it exists, otherwise do nothing
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static PlayScreen resume() throws FileNotFoundException, IOException, ClassNotFoundException {
		File dir = new File(dirPath);
		if (dir.exists()) {
			File fichier =  new File(name);
			if (fichier.exists()) {	
				ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichier)) ;
				PlayScreen p = (PlayScreen)ois.readObject() ;
				ois.close();
				return p;
			}
		}
	return null;
	}
	
	/**
	 * execute the serialization of the playscreen
	 * @param fichier
	 * @param p
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void execute(File fichier, PlayScreen p) throws FileNotFoundException, IOException {
		ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichier)) ;
		oos.writeObject(p);
		oos.close();
	}
	

}

