
package uvsq.m1info.roguelike;

import java.io.Serializable;
import java.lang.reflect.Method;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uvsq.m1info.roguelike.agents.BatAi;
import uvsq.m1info.roguelike.agents.CannibalisticZombieAi;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.agents.FamiliarAi;
import uvsq.m1info.roguelike.agents.FungusAi;
import uvsq.m1info.roguelike.agents.GoblinAi;
import uvsq.m1info.roguelike.agents.MerchantAi;
import uvsq.m1info.roguelike.agents.MimicAi;
import uvsq.m1info.roguelike.agents.PlayerAi;
import uvsq.m1info.roguelike.agents.StrongZombieAi;
import uvsq.m1info.roguelike.agents.ZombieAi;
import uvsq.m1info.roguelike.agents.ZombieSummonerAi;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.World;

import asciiPanel.AsciiPanel;

/**
 * Generates items and creatures.
 */
public class StuffFactory implements Serializable {
	private static final long serialVersionUID = -5270808718031777389L;
	/**
	 * The world in which the factory can create the objects.
	 */
	private World world;
	/**
	 * Map storing the name of the colored potion with the associated colors.
	 */
	private static Map<String, Color> potionColors;
	/**
	 * List used to associate a random Color to a specifid potion.
	 */
	private static List<String> potionAppearances;

	/**
	 * Constructor of the factory.
	 * @param world in which the items/creatures will be created.
	 */
	public StuffFactory(World world) {
		this.world = world;
		setUpPotionAppearances();
	}
	
	/**
	 * Enable the association of each different potion with a random color in the list.
	 */
	private static void setUpPotionAppearances() {
		potionColors = new HashMap<String, Color>();
		potionColors.put("red potion", AsciiPanel.brightRed);
		potionColors.put("yellow potion", AsciiPanel.brightYellow);
		potionColors.put("green potion", AsciiPanel.brightGreen);
		potionColors.put("cyan potion", AsciiPanel.brightCyan);
		potionColors.put("blue potion", AsciiPanel.brightBlue);
		potionColors.put("magenta potion", AsciiPanel.brightMagenta);
		potionColors.put("dark potion", AsciiPanel.brightBlack);
		potionColors.put("grey potion", AsciiPanel.white);
		potionColors.put("light potion", AsciiPanel.brightWhite);
		potionColors.put("purple potion", AsciiPanel.magenta);
		potionAppearances = new ArrayList<String>(potionColors.keySet());
		Collections.shuffle(potionAppearances);
	}
			
	/**
	 * Create an item in the point (x,y,z) with the static method called "new"+name() with the Reflection.
	 * @param name name of the method to call
	 * @param x 
	 * @param y
	 * @param z
	 * @return the new item
	 */
	public static Item newItem(String name, int x, int y, int z) {
		Item item = null;
		String methodName = "new"+ name;
		
		Class<StuffFactory> klass = StuffFactory.class;
		Class[] parameterTypes = new Class[0];
		try {
			Method method= klass.getDeclaredMethod(methodName, parameterTypes);
			method.setAccessible(true);
			
			Object[] args= new Object[0];
			item = (Item)method.invoke(null, args);
			
			item.setLocation(x, y, z);			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return item;
	}
	
	/**
	 * Create an item at a certain depth with the static method called "new"+name() with the Reflection.
	 * @param name name of the method to call
	 * @param z the depth at which to put the new item
	 * @return new item
	 */
	public Item newItem(String name, int z) {
		Item item = null;
		String methodName = "new"+name;
		
		Class<StuffFactory> klass = StuffFactory.class;
		Class[] parameterTypes = new Class[0];
		try {
			Method method = klass.getDeclaredMethod(methodName, parameterTypes);
			method.setAccessible(true);

			Object[] args = new Object[0];
			item = (Item)method.invoke(this, args);
			
			world.addAtEmptyLocation(item, z);	
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return item;
	}
	
	/**
	 * Create a new creature "name" at a certain depth
	 * @param name name of the method to call
	 * @param depth at which to put the new creature
	 * @return new creature
	 */
	public Creature newCreature(String name, int depth) {
		Creature creature = null;
		String methodName = "new"+name;
		
		Class<StuffFactory> klass = StuffFactory.class;
		Class[] parameterTypes = new Class[0];
		try {
			Method method = klass.getDeclaredMethod(methodName, parameterTypes);
			method.setAccessible(true);

			Object[] args = new Object[0];
			creature = (Creature)method.invoke(this, args);
			
			world.addAtEmptyLocation(creature, depth);	
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return creature;
	}

	/**
	 * Create a new creature aggressive creature "name" at a certain depth
	 * @param name name of the method to call
	 * @param depth at which to put the new creature
	 * @param target the player who they will track
	 * @return the new creature
	 */
	public Creature newAggressiveCreature(String name, int depth, Creature target) {
		Creature creature = null;
		if (name.contains("ZombieSummoner")) 
			creature = newZombieSummoner(target);
		else {
			String methodName = "new"+name;
			
			Class<StuffFactory> klass = StuffFactory.class;
			Class[] parameterTypes = new Class[1];
			parameterTypes[0] = Creature.class;
			try {
				Method method = klass.getDeclaredMethod(methodName, parameterTypes);
				method.setAccessible(true);
	
				Object[] args = new Object[1];
				args[0] = target;
				creature = (Creature)method.invoke(null, args);
			}
			catch(Exception ex) {
				ex.printStackTrace();
	
			}
		}
		creature.setWorld(world);
		world.addAtEmptyLocation(creature, depth);	
		if (name.contains("Goblin")) {
			creature.equip(newItem("RandomWeapon", depth));
			creature.equip(newItem("RandomArmor", depth));
		}
		return creature;
	}

	/**
	 * Instanciates a player.
	 * @param messages the list of messages to display
	 * @param fov the field of view of the player
	 * @return the new player
	 */
	public Creature newPlayer(List<String> messages, FieldOfView fov) {
		Creature player = new Creature(world, '@', AsciiPanel.brightWhite, "player", 500, 10, 5);
		world.addAtEmptyLocation(player, 0);
		new PlayerAi(player, messages, fov);
		return player;
	}
	
	/**
	 * Instanciates a standard merchant with a key and random food in his shop
	 * @return the new merchant
	 */
	public Creature newMerchant() {
		Creature merchant = new Creature(world, 'm', AsciiPanel.brightYellow, "merchant", 400, 30, 10);
		merchant.setMoney(100);
		merchant.setStockRoom(20);
		new MerchantAi(merchant);
		merchant.addItemToStock(newKey(), 100);
		merchant.addItemToStock(newFood(), 10);
		return merchant;
	}
	
	/**
	 * * Instanciates a basic merchant with the standard items and a random tool
	 * @return a new merchant
	 */
	public Creature newNoobMerchant() {
		Creature merchant = newMerchant();
		merchant.addItemToStock(newTool(), 200);
		merchant.addItemToStock(newRandomArmor(), 100);
		merchant.addItemToStock(newRandomWeapon(), 100);
		return merchant;
	}

	/**
	 * * Instanciates an expert merchant with the standard items and a random relic and two random potions
	 * @return a new merchant
	 */
	public Creature newExpertMerchant() {
		Creature merchant = newMerchant();
		merchant.addItemToStock(StuffFactory.newRandomRelic(), 500);
		merchant.addItemToStock(newRandomPotion(), 100);
		merchant.addItemToStock(newRandomPotion(), 100);

		return merchant;
	}

	/**
	 * Instanciates a new Familiar
	 * @return the new familiar
	 */
	public Creature newFamiliar() {
		Creature pet = new Creature(world, 'a', AsciiPanel.red, "wild rat", 200, 20, 5);
		new FamiliarAi(pet);
		return pet;
	}

	/**
	 * Instanciates a new merchant at a random position at the selected depth.
	 * @param depth where to add the merchant
	 * @return the new merchant
	 */
	public Creature selectNewMerchant(int depth) {
		if (depth < world.depth() - 2)
			return newCreature("NoobMerchant", depth);
		else
			return newCreature("ExpertMerchant", depth);
	}

	/**
	 * Instanciates a new fungus in the world
	 * @return the new creature
	 */
	public Creature newFungus() {
		Creature fungus = new Creature(world, 'f', AsciiPanel.green, "fungus", 10, 0, 0);
		new FungusAi(fungus, this);
		fungus.setMoney(5);
		return fungus;
	}

	/**
	 * Instanciates a new bat in the world
	 * @return the new creature
	 */
	public Creature newBat() {
		Creature bat = new Creature(world, 'b', AsciiPanel.brightYellow, "bat", 15, 5, 0);
		new BatAi(bat);
		bat.setMoney(10);
		return bat;
	}
	
	/**
	 * Instanciates a new summoned bat
	 * @return the new creature
	 */
	public static Creature newSummonedBat() {
		Creature bat = new Creature(null, 'b', AsciiPanel.brightYellow, "bat", 15, 5, 0);
		new BatAi(bat);
		bat.setMoney(10);
		return bat;
	}
	
	/**
	 * Instanciates a new zombie
	 * @param player the target of the zombie
	 * @return the new zombie
	 */
	public static Creature newZombie(Creature player) {
		Creature zombie = new Creature(null, 'z', AsciiPanel.white, "zombie", 30, 10, 10);
		new ZombieAi(zombie, player);
		zombie.setMoney(15);
		return zombie;
	}
	
	/**
	 * Instanciates a new strong zombie which isn't as slow as the basic zombie
	 * @param player its target
	 * @return the new zombie
	 */
	public static Creature newStrongZombie(Creature player) {
		Creature zombie = new Creature(null, 'z', AsciiPanel.brightMagenta, "strong zombie", 120, 1, 5);
		new StrongZombieAi(zombie, player);
		zombie.setMoney(40);
		return zombie;
	}
	
	/**
	 * A cannibalistic zombie which targets both zombie in its surrounding. Target the player in priority
	 * @param player its target
	 * @return the new zombie
	 */
	public static Creature newCannibalisticZombie(Creature player) {
		Creature zombie = new Creature(null, 'z', AsciiPanel.brightYellow, "cannibalistic zombie", 10, 1, 1);
		new CannibalisticZombieAi(zombie, player);
		zombie.setMoney(20);
		return zombie;
	}

	/**
	 * A weak zombie
	 * @param player its target
	 * @return the new tombie
	 */
	 	public static Creature newLesserZombie( Creature player) {
		Creature zombie = new Creature(null, 'z', Color.GRAY, "lesser zombie", 1, 1, 1);
		new ZombieAi(zombie, player);
		zombie.setMoney(1);
		return zombie;
	}

	/**
	 * Instanciates a zombie from a corpse
	 * @param player the creature to target
	 * @param corpse the corpse to revive from
	 * @return
	 */
	static public Creature newZombieFromCorpse(Creature player, Item corpse) {
		Creature zombie = null;
		if (corpse.name().contains("strong")) {
			zombie = StuffFactory.newStrongZombie(player);
		}else if (corpse.name().contains("lesser")) {
			return null;
		} else if (corpse.name().contains("cannibalistic")) {
			zombie = StuffFactory.newCannibalisticZombie(player);
		}
		else {
			zombie = StuffFactory.newZombie(player);
		}
		zombie.setLocation(corpse.getX(), corpse.getY(), corpse.getZ());
		return zombie;
	}
	
	/**
	 * Zombie summoner that summon zombies when he can see you and flee from you
	 * @param player
	 * @return 
	 */
	private Creature newZombieSummoner(Creature player) {
		Creature zombie = new Creature(null, 'n', Color.DARK_GRAY, "necromancer", 100, 1, 20);
		new ZombieSummonerAi(zombie, player, this);
		zombie.setMoney(100);
		zombie.inventory().add(newVictoryItem());
		return zombie;
	}
	
	/**
	 * A goblin which can pick up equipments and equip them
	 * @param player
	 * @return
	 */
	public static Creature newGoblin(Creature player) {
		Creature goblin = new Creature(null, 'g', AsciiPanel.brightGreen, "goblin", 66, 15, 5);
		new GoblinAi(goblin, player);
		goblin.setMoney(25);
		return goblin;
	}
	
	/**
	 * A mimic at the location (x,y,z) which can drop a random cursedrelic
	 * @param x
	 * @param y
	 * @param z
	 * @param player
	 * @return
	 */
	public static Creature newMimic(int x, int y, int z, Creature player) {
		Creature mimic = new Creature(null, 'c', AsciiPanel.yellow, "mimic", 200, 15, 5);
		new MimicAi(mimic, player);
		mimic.setLocation(x, y, z);
		if (Math.random() < 0.4)
			mimic.inventory().add(newRandomCursedRelic());
		mimic.inventory().add(newFood());
		mimic.setMoney(75);
		return mimic;
	}
	
	/**
	 * A rock
	 * @return
	 */
	public static Item newRock() {
		Item rock = new Item(',', AsciiPanel.yellow, "rock");
		return rock;
	}
	
	/**
	 * The victory item to retrieve to win the game
	 * @return
	 */
	private Item newVictoryItem() {
		Item item = new Item('*', AsciiPanel.brightWhite, "exit key");
		return item;
	}

	/**
	 * A torch which can light its surroundings
	 * @param depth
	 * @return
	 */
	public static Item newTorch() {
		Item torch = new Item('i', AsciiPanel.brightYellow, "torchlight");
		torch.setDurationValue(40);
		return torch;
	}

	/**
	 * A pickaxe which make it easier to mine the walls. Breaks after 100 uses
	 * @return
	 */
	public static Item newPickaxe() {
		Item pickaxe = new Item('p', AsciiPanel.red, "pickaxe");
		pickaxe.setMiningValue(2);
		pickaxe.setDurationValue(100);
		return pickaxe;
	}

	/**
	 * A shovel which enables to go down a level if the map permits it. Can be used 3 times
	 * @return
	 */
	public static Item newShovel() {
		Item shovel = new Item('s', AsciiPanel.red, "shovel");
		shovel.setDurationValue(3);
		return shovel;
	}

	/**
	 * Instanciates a random tool. A pickaxe is generated with a bigger probability
	 * @return
	 */
	public static Item newTool() {
		if (Math.random() < 0.8) 
			return newPickaxe();
		return newShovel();
	}
	
	/**
	 * A key. Used to open Chests
	 * @return
	 */
	public Item newKey() {
		Item item = new Item('k', AsciiPanel.brightYellow, "key");
		return item;
	}
	
	/**
	 * A fruit. Used as food
	 * @return
	 */
	public static Item newFruit() {
		Item item = new Item('%', AsciiPanel.brightRed, "apple");
		item.modifyFoodValue(10);
		return item;
	}
	
	/**
	 * Some bread. Used as food
	 * @return
	 */
	public static Item newBread() {
		Item item = new Item('%', AsciiPanel.yellow, "bread");
		item.modifyFoodValue(40);
		return item;
	}
	
	/**
	 * Instianciates random food.
	 * @return
	 */
	public static Item newFood() {
		switch((int)(Math.random()) * 2) {
		case 0 : return newFruit();
		default : return newBread();
		}
	}

	/**
	 * A dagger
	 * @return
	 */
	public static Item newDagger() {
		Item item = new Item(')', AsciiPanel.white, "dagger");
		item.modifyAttackValue(5);
		item.setDurationValue(30);
		return item;
	}

	/**
	 * A sword
	 * @return
	 */
	public static Item newSword() {
		Item item = new Item(')', AsciiPanel.brightWhite, "sword");
		item.modifyAttackValue(10);
		item.setDurationValue(30);
		return item;
	}

	/**
	 * A staff
	 * @return
	 */
	public static Item newStaff() {
		Item item = new Item(')', AsciiPanel.yellow, "staff");
		item.modifyAttackValue(5);
		item.modifyDefenseValue(3);
		item.setDurationValue(30);
		return item;
	}

	/**
	 * A bow
	 * @return
	 */
	public static Item newBow() {
		Item item = new Item(')', AsciiPanel.yellow, "bow");
		item.modifyAttackValue(1);
		item.modifyRangedAttackValue(5);
		item.setDurationValue(30);
		return item;
	}

	/**
	 * A light armor
	 * @return
	 */
	private static Item newLightArmor() {
		Item item = new Item('[', AsciiPanel.green, "tunic");
		item.modifyDefenseValue(2);
		item.setDurationValue(30);
		return item;
	}

	/**
	 * A medium armor
	 * @return
	 */
	private static Item newMediumArmor() {
		Item item = new Item('[', AsciiPanel.white, "chainmail");
		item.modifyDefenseValue(4);
		item.setDurationValue(30);
		return item;
	}

	/**
	 * A heavy armor
	 * @return
	 */
	private static Item newHeavyArmor() {
		Item item = new Item('[', AsciiPanel.brightWhite, "platemail");
		item.modifyDefenseValue(6);
		item.setDurationValue(30);
		return item;
	}

	/**
	 * Instanciates a random weapon
	 * @return
	 */
	public static Item newRandomWeapon() {
		switch ((int) (Math.random() * 3)) {
		case 0 : return newDagger();
		case 1 : return newSword();
		case 2 : return newBow();
		default : return newStaff();
		}
	}

	/**
	 * Instanciates a random armor
	 * @return
	 */
	public static Item newRandomArmor() {
		switch ((int) (Math.random() * 3)) {
		case 0 : return newLightArmor();
		case 1 : return newMediumArmor();
		default : return newHeavyArmor();
		}
	}


	/**
	 * A healing potion
	 * @return
	 */
	private static Item newPotionOfHealth() {
		String appearance = potionAppearances.get(0);
		final Item item = new Item('!', potionColors.get(appearance), "health potion");
		item.setQuaffEffect(Effect.HEALTH);
		return item;
	}

	/**
	 * A mana restoring potion
	 * @return
	 */
	private static Item newPotionOfMana() {
		String appearance = potionAppearances.get(1);
		final Item item = new Item('!', potionColors.get(appearance), "mana potion");
		item.setQuaffEffect(Effect.MANA);
		return item;
	}

	/**
	 * A potion that restores hp during several turns
	 * @return
	 */
	private static Item newPotionOfSlowHealth() {
		String appearance = potionAppearances.get(2);
		final Item item = new Item('!', potionColors.get(appearance), "slow health potion");
		item.setQuaffEffect(Effect.SLOWHEALTH);
		return item;
	}

	/**
	 * A potion that causes damage to the player during several turns
	 * @return
	 */
	private static Item newPotionOfPoison() {
		String appearance = potionAppearances.get(3);
		final Item item = new Item('!', potionColors.get(appearance), "poison potion");
		item.setQuaffEffect(Effect.POISON);
		return item;
	}

	/**
	 * A potion that increases the player's stats during several turns
	 * @return
	 */
	private static Item newPotionOfWarrior() {
		String appearance = potionAppearances.get(4);
		final Item item = new Item('!', potionColors.get(appearance), "warrior's potion");
		item.setQuaffEffect(Effect.WARRIOR);
		return item;
	}

	/**
	 * A potion that increases the player's vision radius during several turns
	 * @return
	 */
	private static Item newPotionOfArcher() {
		String appearance = potionAppearances.get(5);
		final Item item = new Item('!', potionColors.get(appearance), "archers potion");
		item.setQuaffEffect(Effect.ARCHER);
		return item;
	}

	
	/**
	 * A potion that increases the player's xp
	 * @return
	 */
	private static Item newPotionOfExperience() {
		String appearance = potionAppearances.get(6);
		final Item item = new Item('!', potionColors.get(appearance), "experience potion");
		item.setQuaffEffect(Effect.XPBOOST);
		return item;
	}

	/**
	 * A potion that enables to see the whole map (at the same depth) during several turns
	 * @return
	 */
	private static Item newPotionOfVision() {
		String appearance = potionAppearances.get(7);
		final Item item = new Item('!', potionColors.get(appearance), "vision potion");
		item.setQuaffEffect(Effect.VISION);
		return item;
	}

	
	/**
	 * Instanciate a random potion
	 * @return
	 */
	public static Item newRandomPotion() {
		switch ((int) (Math.random() * 10)) {
		case 0:
			return newPotionOfHealth();
		case 1:
			return newPotionOfHealth();
		case 2:
			return newPotionOfMana();
		case 3:
			return newPotionOfMana();
		case 4:
			return newPotionOfSlowHealth();
		case 5:
			return newPotionOfPoison();
		case 6:
			return newPotionOfWarrior();
		case 7:
			return newPotionOfArcher();
		case 8:
			return newPotionOfVision();
		default:
			return newPotionOfExperience();
		}
	}


	/**
	 * A white mage spell's book that allow the use of several spells
	 * @return
	 */
	private static Item newWhiteMagesSpellbook() {
		Item item = new Item('+', AsciiPanel.brightWhite, "white mage's spellbook");
		item.addWrittenSpell("minor heal", 4, Effect.MINORHEAL, true);
		item.addWrittenSpell("major heal", 8, Effect.MAJORHEAL, true);
		item.addWrittenSpell("slow heal", 12, Effect.SLOWHEAL, false);
		item.addWrittenSpell("inner strength", 16, Effect.INNERSTRENGTH, false);
		return item;
	}

	/**
	 * A blue mage spell's book that allow the use of several spells
	 * @return
	 */
	private static Item newBlueMagesSpellbook() {
		Item item = new Item('+', AsciiPanel.brightBlue, "blue mage's spellbook");
		item.addWrittenSpell("blood to mana", 1, Effect.BLOODTOMANA, true);
		item.addWrittenSpell("blink", 6, Effect.BLINK, false);
		item.addWrittenSpell("summon bats", 11, Effect.SUMMONBATS, false);
		item.addWrittenSpell("detect creatures", 16, Effect.DETECTCREATURES, false);
		return item;
	}

	/**
	 * Instanciates a random mage's spell
	 * @return
	 */
	public static Item newRandomSpellBook() {
		switch ((int) (Math.random() * 2)) {
		case 0:
			return newWhiteMagesSpellbook();
		default:
			return newBlueMagesSpellbook();
		}
	}

	/**
	 * A weapon relic with a big attack value;
	 * @return
	 */
	public static Item newOrichalcumSword() {
		Item item = new Item(')', AsciiPanel.brightYellow, "orichalcum sword");
		item.modifyAttackValue(30);
		return item;
	}

	/**
	 * A weapon relic which doubles the gain of money when equipped
	 * @return
	 */
	private static Item newSwordOfFortune() {
		Item item = new Item(')', AsciiPanel.brightYellow, "sword of fortune");
		item.modifyAttackValue(10);
		item.multiplyCoin(2);
		return item;
	}

	/**
	 * An armor relic which increases the wearer's maxHp when equipped;
	 * @return
	 */
	private static Item newArmorOfVitality() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of vitality");
		item.modifyDefenseValue(15);
		item.modifyAttackValue(1);
		item.setMaxHpBonus(100);
		return item;
	}

	/**
	 * An armor relic which doubles the gain of money when equipped
	 * @return
	 */
	private static Item newArmorOfWealth() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of wealth");
		item.modifyDefenseValue(8);
		item.multiplyCoin(2);
		return item;
	}
	
	/**
	 * An armor relic which increases the number of lives when equipped for the first time
	 * @return
	 */
	private static Item newArmorOfImmortality() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of immortality");
		item.modifyDefenseValue(10);
		item.setAdditionalLives(1);
		return item;
	}
	
	/**
	 * Instanciates a random relic
	 * @return
	 */
	public static Item newRandomRelic() {
		switch ((int) (Math.random() * 5)) {
		case 0:
			return newOrichalcumSword();
		case 1:
			return newArmorOfVitality();
		case 2:
			return newArmorOfImmortality();
		case 3:
			return newSwordOfFortune();
		default:
			return newArmorOfWealth();
		}
	}

	/**
	 * A cursed armor relic which decreases the wearer's max hp when equipped
	 * @return
	 */
	private static Item newArmorOfSickness() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of Sickness");
		item.modifyDefenseValue(40);
		item.setMaxHpBonus(-70);
		return item;
	}

	/**
	 * A cursed weapon relic which divises the wearer's money gain by half when equipped

	 * @return
	 */
	private static Item newSwordOfMisfortune() {
		Item item = new Item(')', AsciiPanel.brightYellow, "sword of misfortune");
		item.modifyAttackValue(50);
		item.multiplyCoin(0.5);
		return item;
	}

	/**
	 * Instanciates a random cursed relic
	 * @return
	 */
	public static Item newRandomCursedRelic() {
		switch ((int) (Math.random() * 2)) {
		case 0:
			return newArmorOfSickness();
		default:
			return newSwordOfMisfortune();
		}
	}
}