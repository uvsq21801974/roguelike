package uvsq.m1info.roguelike.screens.target;

import uvsq.m1info.roguelike.Spell;
import uvsq.m1info.roguelike.agents.Creature;
/**
 * Cast spell Screen to allows the cast of a spell at a certain coordinate
 */
public class CastSpellScreen extends TargetBasedScreen {
	private Spell spell;
	
	public CastSpellScreen(Creature player, String caption, int sx, int sy, Spell spell) {
		super(player, caption, sx, sy);
		this.spell = spell;
	}
	
	@Override
	public void selectWorldCoordinate(int x, int y, int screenX, int screenY){
		player.castSpell(spell, x, y);
	}
}
