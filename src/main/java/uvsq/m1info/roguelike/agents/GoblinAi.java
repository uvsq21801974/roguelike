package uvsq.m1info.roguelike.agents;


public class GoblinAi extends CreatureAi {
	private static final long serialVersionUID = 8165118816621864446L;
	private Creature player;
	
	/**
	 * Behaviour of a goblin that can equip items and hunt the player if he is in its view
	 * @param creature
	 * @param player
	 */
	public GoblinAi(Creature creature, Creature player) {
		super(creature);
		this.player = player;
	}

	public void onUpdate(){
		if (canUseBetterEquipment())
			useBetterEquipment();
		else if (canRangedWeaponAttack(player))
			creature.rangedWeaponAttack(player);
		else if (creature.canSee(player.x, player.y, player.z))
			hunt(player);
		else if (canPickup())
			creature.pickup();
		else
			wander();
	}
}
