package uvsq.m1info.roguelike.agents;


public class PNJAi extends CreatureAi {
	private static final long serialVersionUID = 6273350748869895425L;
	protected Creature target;
	@Override
	public Creature target() { return target; }
	
	/**
	 * Constructor of a pnj
	 * @param creature
	 */
	public PNJAi(Creature creature) {
		super(creature);
	}
	
	@Override
	public void attackedBy(Creature creature) {
		target = creature;
	}
	
	@Override
	public void onUpdate(){
		if (target != null && creature.canSee(target.x, target.y, target.z))
			hunt(target);
		else target = null;
	}
	
	
}
