package uvsq.m1info.roguelike.screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

/**
 * Help Menu
 */
public class HelpScreen implements Screen {

	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.clear();
		terminal.writeCenter("Help menue", 1);
		terminal.write("Fight the necromancer in the depths of the caves and get the exit key to get out", 1, 3);
		
		int y = 6;
		terminal.write("[?] for help", 2, y++);
		terminal.write("[,] to pick up", 2, y++);
		terminal.write("[n] to drop", 2, y++);
		terminal.write("[r] to eat", 2, y++);
		terminal.write("[y] to wear or wield", 2, y++);
		terminal.write("[f] to examine your items", 2, y++);
		terminal.write("[;] to look around", 2, y++);
		terminal.write("[t] to quaff a potion", 2, y++);
		terminal.write("[g] to read something", 2, y++);
		terminal.write("[o] to interract if a pnj", 2, y++);
		
		terminal.writeCenter("-- press any key to continue --", 22);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return null;
	}
}
