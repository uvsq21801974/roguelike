package uvsq.m1info.roguelike.agents;

import uvsq.m1info.roguelike.StuffFactory;

/**
 * @author dell
 *
 */
public class ZombieSummonerAi extends CreatureAi {
	private static final long serialVersionUID = -6342259167818941911L;

	/**
	 * player it will flee from
	 */
	private Creature player;
	/**
	 * counts the turn at which the creature will summon lesserzombies
	 */
	private int cpt;
	/**
	 * the factory with which the creature will summon new monsters
	 */
	private StuffFactory factory;
	
	/**
	 * COnstructor
	 * @param creature
	 * @param player
	 * @param factory
	 */
	public ZombieSummonerAi(Creature creature, Creature player, StuffFactory factory) {
		super(creature);
		this.player = player;
		this.factory = factory;
		this.cpt = 0;
	}

	@Override
	public void onUpdate(){
		cpt ++;
		if (cpt%5 == 0 && player.canSee(creature.x, creature.y, creature.z))
			summon();
		if (Math.random() < 0.2)
			return;
		if (creature.canSee(player.x, player.y, player.z)) {
			flee(player);
		}
		else
			wander();
	}
	
	/**
	 * Summon a weak zombie in its surroundings if the player is in his sight each 5 turns
	 */
	protected void summon() {
		if(factory == null) 
			return;
		int x = creature.x + (int)(Math.random() * 5) - 3;
		int y = creature.y + (int)(Math.random() * 5) - 3;
		
		if (!creature.canEnter(x, y, creature.z))
			return;

		creature.doAction("spawn a child");
		Creature child = factory.newAggressiveCreature("LesserZombie", creature.z, player);
		child.setLocation(x, y, creature.z);
	}
}
