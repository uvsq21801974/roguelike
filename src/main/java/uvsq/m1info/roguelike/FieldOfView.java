package uvsq.m1info.roguelike;

import java.io.Serializable;

import uvsq.m1info.roguelike.map.Line;
import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.map.Tile;
import uvsq.m1info.roguelike.map.World;

/**
 * Enables the discovery of the map with the player.
 *
 */
public class FieldOfView implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9085740162127002767L;
	/**
	 * The world in which operates the field of view
	 */
	private World world;
	/**
	 * The depth in which operate the field of view
	 */
	private int depth;
	/**
	 * The tiles that can be seen in the direct surroundings of the player
	 */
	private boolean[][] visible;
	/**
	 * The depth at which the character started the spell of vision
	 */
	private int depthVision;
	
	/**
	 * Return true if the location (x, y, z) is in the direct vision of the player, false otherwise
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public boolean isVisible(int x, int y, int z){
		return z == depth && 
					(( x >= 0 && y >= 0 && x < visible.length &&
					y < visible[0].length && visible[x][y] )) ;
	}

	/**
	 * Tiles discovered in the map, is used as a backup when a potion of vision is used
	 */
	private Tile[][][] tilesTmp;
	
	/**
	 * Tiles displayed in the map
	 */
	private Tile[][][] tiles;
	public Tile tile(int x, int y, int z){
		if (x < 0 || x >= world.width() || y < 0 || y >= world.height())
			return null;
		return tiles[x][y][z];
	}
	
	/**
	 * Constructor of fov. The fov is instanciated with no Tiles known
	 * @param world
	 */
	public FieldOfView(World world){
		this.world = world;
		this.visible = new boolean[world.width()][world.height()];
		this.tiles = new Tile[world.width()][world.height()][world.depth()];
		this.tilesTmp = new Tile[world.width()][world.height()][world.depth()];
		
		for (int x = 0; x < world.width(); x++){
			for (int y = 0; y < world.height(); y++){
				for (int z = 0; z < world.depth(); z++){
					tiles[x][y][z] = Tile.UNKNOWN;
					tilesTmp[x][y][z] = Tile.UNKNOWN;

				}
			}
		}
	}
	
	/**
	 * Add the new region with center (x, y, z) and a radius of r to the list of known tiles
	 * @param wx
	 * @param wy
	 * @param wz
	 * @param r
	 */
	public void update(int wx, int wy, int wz, int r){
		depth = wz;
		visible = new boolean[world.width()][world.height()];
		
		for (int x = -r; x < r; x++){
			for (int y = -r; y < r; y++){
				if (x * x + y * y > r * r)
					continue;
				
				if (wx + x < 0 || wx + x >= world.width() || wy + y < 0 || wy + y >= world.height())
					continue;
				
				for (Point p : new Line(wx, wy, wx + x, wy + y)){
					Tile tile = world.tile(p.x, p.y, wz);
					visible[p.x][p.y] = true;
					tilesTmp[p.x][p.y][wz] = tile; 
					tiles[p.x][p.y][wz] = tile; 
						
					if (!tile.isGround())
						break;
				}
			}
		} 
	}
	
	/**
	 * When notified with this method, the field of view contains the whole level
	 * @param depth
	 */
	public void startVisibility(int depth){
		depthVision = depth;
		for (int x = 0; x < world.width(); x++){
			for (int y = 0; y < world.height(); y++){
				tiles[x][y][depth] = world.tile(x, y ,depth);
			}
		}
	}
	
	/**
	 * When notified with this method, the field of view switches back to its normal state
	 * @param depth
	 */
	public void endVisibility(){
		for (int x = 0; x < world.width(); x++){
			for (int y = 0; y < world.height(); y++){
				tiles[x][y][depthVision] = tilesTmp[x][y][depthVision];
			}
		}
	}
}
