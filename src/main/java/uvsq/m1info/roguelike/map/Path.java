package uvsq.m1info.roguelike.map;

import java.util.List;

import uvsq.m1info.roguelike.agents.Creature;

public class Path {

	/**
	 * path finder
	 */
	private static PathFinder pf = new PathFinder();
	
	/**
	 * positions in the path
	 */
	private List<Point> points;
	/**
	 * the points in the calculated path
	 * @return
	 */
	public List<Point> points() { return points; }
	/**
	 * Constructor of a path from the creature to the position (x,y,creature.z)
	 * @param creature
	 * @param x
	 * @param y
	 */
	public Path(Creature creature, int x, int y){
		points = pf.findPath(creature, new Point(creature.x, creature.y, creature.z), new Point(x, y, creature.z), 300);
	}
}
