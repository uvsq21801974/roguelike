package uvsq.m1info.roguelike.items;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import uvsq.m1info.roguelike.Effect;
import uvsq.m1info.roguelike.Spell;
import uvsq.m1info.roguelike.map.Location;
import uvsq.m1info.roguelike.map.Point;

public class Item implements Location, Serializable {
	private static final long serialVersionUID = -6960457382915534897L;
	/**
	 * symbol of the item
	 */
	private char glyph;
	public char glyph() { return glyph; }
	
	/**
	 * color of the item
	 */
	private Color color;
	public Color color() { return color; }

	/**
	 * name of the item
	 */
	private String name;
	public String name() { return name; }
	
	/**
	 * food value of the item
	 */
	private int foodValue;
	public int foodValue() { return foodValue; }
	public void modifyFoodValue(int amount) { foodValue += amount; }

	/**
	 * attack value of the item
	 */
	private int attackValue;
	public int attackValue() { return attackValue; }
	public void modifyAttackValue(int amount) { attackValue += amount; }

	/**
	 * defense value of the item
	 */
	private int defenseValue;
	public int defenseValue() { return defenseValue; }
	public void modifyDefenseValue(int amount) { defenseValue += amount; }

	/**
	 * ranged attack value of the item
	 */
	private int rangedAttackValue;
	public int rangedAttackValue() { return rangedAttackValue; }
	public void modifyRangedAttackValue(int amount) { rangedAttackValue += amount; }
	
	/**
	 * maxHpBonus of the item
	 */
	private int maxHpBonus;
	public int maxHpBonus() { return maxHpBonus; }
	public void setMaxHpBonus(int value) { maxHpBonus = value; }
	
	/**
	 * coinMultiplicator of the item
	 */
	private double coinMultiplicator;
	public double coinMultiplicator() { return coinMultiplicator; }
	public void multiplyCoin(double value) { coinMultiplicator *= value; }

	/**
	 * additionalLives of the item
	 */
	private int additionalLives;
	public int additionalLives() { return additionalLives; }
	public void setAdditionalLives(int value) { additionalLives = value; }
	
	/**
	 * effects of the item
	 */
	private Effect quaffEffect;
	public Effect quaffEffect() { return quaffEffect; }
	public void setQuaffEffect(Effect effect) { this.quaffEffect = effect; }
	
	/**
	 * List of spells in the book
	 */
	private List<Spell> writtenSpells;
	public List<Spell> writtenSpells() { 
		return writtenSpells; 
	}
	/**
	 * add a spell to the book
	 * @param name
	 * @param manaCost
	 * @param effect
	 * @param target
	 */
	public void addWrittenSpell(String name, int manaCost, Effect effect, boolean target){
		writtenSpells.add(new Spell(name, manaCost, effect, target));
	}
	
	/**
	 * mining value of the item
	 */
	private int miningValue;
	public int miningValue(){ return miningValue; }
	public void setMiningValue(int amount){ miningValue = amount; }
	
	/**
	 * number of turns the item will take to disappear (equipment and corpse)
	 */
	private int durationValue = 0;

	/**
	 * decrease the duration value of the item
	 */
	public void decreaseDurationValue() { 
		durationValue--; 
	}
	
	/**
	 * set the duration value (equipment only)
	 */
	public void setDurationValue(int durationValue) { 
		this.durationValue = durationValue;
	}
	
	/**
	 * reset the duration value (equipment only)
	 */
	public void resetDurationValue() { 
		durationValue = 10; 
	}
	
	/**
	 * check if the item is a zombie corpse
	 * @return
	 */
	public boolean isZombieCorpse() {
		return name.contains("zombie corpse");
	}

	/**
	 * check if the item can be revived from
	 * @return
	 */
	public boolean canRevive(){
		if(isZombieCorpse()) 
			return durationValue < -1;
		return false;
	}
	
	/**
	 * Constructor
	 */
	public Item(char glyph, Color color, String name){
		this.glyph = glyph;
		this.color = color;
		this.name = name;
		this.writtenSpells = new ArrayList<Spell>();
		this.coinMultiplicator = 1;
		this.maxHpBonus = 0;
		
	}
	
	/**
	 * Information on the item
	 * @return
	 */
	public String details() {
		String details = "";
		
		if (attackValue != 0)
			details += "  attack:" + attackValue;

		if (rangedAttackValue > 0)
			details += "  ranged:" + rangedAttackValue;
		
		if (defenseValue != 0)
			details += "  defense:" + defenseValue;

		if (foodValue != 0)
			details += "  food:" + foodValue;
		
		return details;
	}

	/**
	 * check if is torchlight
	 * @return
	 */
	public boolean isTorch() {
		return name == "torchlight";
	}
	
	/**
	 * check if is weapon
	 * @return
	 */
	public boolean isWeapon() {
		return (attackValue > 0 || rangedAttackValue > 0) && (attackValue + rangedAttackValue > defenseValue); 
	}
	
	/**
	 * check if is armor
	 * @return
	 */
	public boolean isArmor(){
		return defenseValue > 0 && defenseValue > attackValue + rangedAttackValue;
	}
	
	/**
	 * check if is pickaxe
	 * @return
	 */
	public boolean isPickaxe() {
		return miningValue > 0;
	}
	
	/**
	 * check if is shovel
	 * @return
	 */
	public boolean isShovel() {
		return name.contains("shovel");
	}
	
	/**
	 * check if is broken and needs to be get ridden of
	 * @return
	 */
	public boolean isBroken() {
		return durationValue < 0;
	}

	/**
	 * Location of the item, Point or Creature
	 */
	private Location location;
	
	@Override
	public void setLocation(int x, int y, int z) {
		location = new Point(x, y, z);
	}
	@Override
	public Location location() {
		return location;
	}
	@Override
	public void setLocation(Location location) {
		this.location = location;
		
	}
	@Override
	public int getX() {
		return location.getX();
	}
	@Override
	public int getY() {
		return location.getY();
	}
	@Override
	public int getZ() {
		return location.getZ();
	}
	
	//====================== test
	public Location getLocation() {
		return location;
	}
}
