package uvsq.m1info.roguelike.agents;

import uvsq.m1info.roguelike.StuffFactory;
import uvsq.m1info.roguelike.items.Item;

public class MerchantAi extends PNJAi {

	private static final long serialVersionUID = -2272913475065509001L;

	/**
	 * Behaviour of a merchant
	 * @param creature
	 */
	public MerchantAi(Creature creature) {
		super(creature);
	}
	
	@Override
	public void sellItem(Creature player, int itemIndex){
		if (player.inventory().isFull()) {
			player.doAction("can't carry any more objects");
			return;
		}
		int price = creature.price(itemIndex);
		if (player.money() < price) {
			player.doAction("don't have enough money");
			return;
		}
		Item item = creature.stock(itemIndex);
		player.inventory().add(item);
		player.setMoney(-price);
		if (item.name().contains("potion")) {
			creature.inventory().remove(item);
			creature.inventory().add(StuffFactory.newRandomPotion());
		}
		item.setLocation(player);
		player.doAction("acquire a "+item.name());
	}

}

