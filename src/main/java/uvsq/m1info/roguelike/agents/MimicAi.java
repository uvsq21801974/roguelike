package uvsq.m1info.roguelike.agents;

public class MimicAi extends CreatureAi {
	private static final long serialVersionUID = 1889528373937177964L;
	/**
	 * targets the player
	 */
	private Creature player;
	/**
	 * counter that counts the updates at which the mimic moves
	 */
	private int cpt = 0;
	/**
	 * A mimic that follows the player slowly aven if it can't see him
	 * @param creature
	 * @param player
	 */
	public MimicAi(Creature creature, Creature player) {
		super(creature);
		this.player = player;
	}

	@Override
	public void onUpdate(){
		cpt = (cpt + 1)%3;
		if (cpt == 0) {
			hunt(player);
		}
	}
}	
