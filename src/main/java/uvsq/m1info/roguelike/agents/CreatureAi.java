package uvsq.m1info.roguelike.agents;

import java.io.Serializable;
import java.util.List;


import uvsq.m1info.roguelike.LevelUpController;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Line;
import uvsq.m1info.roguelike.map.Path;
import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.map.Tile;

public class CreatureAi implements Serializable {
	private static final long serialVersionUID = -2044591488230660781L;
	/**
	 * Creature linked to the behavior
	 */
	protected Creature creature;
	/**
	 * Last attacker of this creature
	 */
	protected Creature attacker = null;
	
	/**
	 * Constructor of the creature behavior
	 * @param creature
	 */
	public CreatureAi(Creature creature){
		this.creature = creature;
		this.creature.setCreatureAi(this);
	}
	
	/**
	 * If the creature enters a wall, do nothing, else, change location
	 * @param x
	 * @param y
	 * @param z
	 * @param tile
	 */
	public void onEnter(int x, int y, int z, Tile tile){
		if (tile.isGround()){
			creature.setLocation(x, y, z);
		} else {
			creature.doAction("bump into an obstacle");
		}
	}
	
	/**
	 * Behaviour of the creature at each new turn
	 */
	public void onUpdate(){ }
	
	/**
	 * If the creature is a player, the creature stocks the messages else do nothing
	 * @param message
	 */
	public void onNotify(String message){ }

	/**
	 * Check if the location (wx, wy, wz) can be seen by the creature
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public boolean canSee(int wx, int wy, int wz) {
		if (creature.z != wz)
			return false;
		int deltaX = creature.x - wx;
		int deltaY = creature.y - wy;
		if (deltaX * deltaX + deltaY * deltaY > creature.visionRadius() * creature.visionRadius () )
			return false;
		for (Point p : new Line(creature.x, creature.y, wx, wy)){
			if (creature.realTile(p.x, p.y, wz).isGround() || p.x == wx && p.y == wy)
				continue;
			return false;
		}
		return true;
	}
	
	/**
	 * Mova at random in the close cases
	 */
	public void wander(){
    	List<Point> n = Point.neighborsN(1);
    	for (Point neighbor : n){
            Creature other = creature.creature(creature.x + neighbor.x, creature.y + neighbor.y, creature.y);
    		if(other == null || other.name() != creature.name()) {
    			creature.moveBy(neighbor.x, neighbor.y, 0);
    			return;
    		}
    	}
	}

	/**
	 * Apply bonus at the notification of a new level
	 */
	public void onGainLevel() {
		new LevelUpController().autoLevelUp(creature);
	}

	/**
	 * Return the last state of the tile as remembered if the creature is a player
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public Tile rememberedTile(int wx, int wy, int wz) {
		return Tile.UNKNOWN;
	}

	
	/**
	 * Check if can attack other with a ranged weapon
	 * @param other
	 * @return
	 */
	protected boolean canRangedWeaponAttack(Creature other) {
		return creature.weapon() != null
		    && creature.weapon().rangedAttackValue() > 0
		    && creature.canSee(other.x, other.y, other.z);
	}

	/**
	 * check if the creature can pickup an item at the ground
	 * @return
	 */
	protected boolean canPickup() {
		return creature.item(creature.x, creature.y, creature.z) != null
			&& !creature.inventory().isFull();
	}

	/**
	 * the creature starts following the target
	 * @param target
	 */
	public void hunt(Creature target) {
		List<Point> points = new Path(creature, target.x, target.y).points();
		if (points == null || points.size() == 0) {
			creature.moveBy(target.x - creature.x, target.y - creature.y, 0);
			return;
		}
		int mx = points.get(0).x - creature.x;
		int my = points.get(0).y - creature.y;
		creature.moveBy(mx, my, 0);
	}
	
	/**
	 * the last attacker of the creature
	 * @return
	 */
	public Creature attacker() {
		return attacker;
	}
	
	/**
	 * flee the target with a simple algorithm
	 * @param target
	 */
	public void flee(Creature target) {
		int mx = target.x - creature.x;
		if (mx != 0) 
			mx = (target.x - creature.x) / Math.abs(target.x - creature.x);
		int my = target.y - creature.y;
		if (my != 0)
			my = (target.y - creature.y) / Math.abs(target.y - creature.y);
		mx = -mx;
		my = -my;
		creature.moveBy(mx, my, 0);
	}

	/**
	 * check if there are better equipments than the items equipped 
	 * @return
	 */
	protected boolean canUseBetterEquipment() {
		int currentWeaponRating = creature.weapon() == null ? 0 : creature.weapon().attackValue() + creature.weapon().rangedAttackValue();
		int currentArmorRating = creature.armor() == null ? 0 : creature.armor().defenseValue();
		
		for (Item item : creature.inventory().getItems()){
			if (item == null)
				continue;
			
			if (item.attackValue() + item.rangedAttackValue() > currentWeaponRating
					|| item.isArmor() && item.defenseValue() > currentArmorRating)
				return true;
		}
		return false;
	}

	/**
	 * Equip the better equipment in the inventory
	 */
	protected void useBetterEquipment() {
		int currentWeaponRating = creature.weapon() == null ? 0 : creature.weapon().attackValue() + creature.weapon().rangedAttackValue();
		int currentArmorRating = creature.armor() == null ? 0 : creature.armor().defenseValue();
		
		for (Item item : creature.inventory().getItems()){
			if (item == null)
				continue;
			
			boolean isArmor = item.attackValue() + item.rangedAttackValue() < item.defenseValue();
			
			if (item.attackValue() + item.rangedAttackValue() > currentWeaponRating
					|| isArmor && item.defenseValue() > currentArmorRating) {
				creature.equip(item);
			}
		}
	}

	/**
	 * notify that the creature was attacked by someone and save the attacker
	 * @param creature
	 */
	public void attackedBy(Creature creature) {
		attacker = creature;
	}

	/**
	 * Sells item (merchant only) at the index itemIndex in the inventory to creature
	 * @param creature
	 * @param itemIndex
	 */
	public void sellItem(Creature creature, int itemIndex) { }

	/**
	 * return the target of the creature
	 * @return
	 */
	public Creature target(){ 
		return null; 
	}

	/**
	 * the familiar gets tamed by creature
	 * @param creature
	 */
	public void tamedBy(Creature creature) { }

	/**
	 * the master of the familiar
	 * @return
	 */
	public Creature master() { return null; }
}


