package uvsq.m1info.roguelike.agents;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import asciiPanel.AsciiPanel;

import uvsq.m1info.roguelike.Effect;
import uvsq.m1info.roguelike.Spell;
import uvsq.m1info.roguelike.StuffFactory;
import uvsq.m1info.roguelike.items.Inventory;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Line;
import uvsq.m1info.roguelike.map.Location;
import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.map.Tile;
import uvsq.m1info.roguelike.map.World;

/**
 * Class of the agents
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
/**
 * @author dell
 *
 */
public class Creature implements Location, Serializable {
	private static final long serialVersionUID = -2319394183562360953L;
	/**
	 * World in which exists the creature
	 */
	private World world;
	/**
	 * Coordinate of the creature
	 */
	public int x;
	/**
	 * Coordinate of the creature
	 */
	public int y;
	/**
	 * Coordinate of the creature
	 */
	public int z;
	/**
	 * Symbol of the creature
	 */
	private char glyph;
	/**
	 * Name of the creature
	 */
	private String name;
	/**
	 * Color of the creature
	 */
	private Color color;
	/**
	 * Behaviour of the creature
	 */
	private CreatureAi ai;
	/**
	 * Money of the creature
	 */
	private int money;
	/**
	 * AdditionalLives of the creature
	 */
	private int additionalLives = 0;
	/**
	 * MaxHp of the creature
	 */
	private int maxHp;
	/**
	 * Current hp of the creature
	 */
	private int hp;
	/**
	 * MaxMana of the creature
	 */
	private int maxMana;
	/**
	 * Current mana of the creature
	 */
	private int mana;
	/**
	 * MaxFood of the creature
	 */
	private int maxFood;
	/**
	 * Current food of the creature
	 */
	private int food;
	/**
	 * Attack value of the creature
	 */
	private int attackValue;
	/**
	 * Defense Value of the creature
	 */
	private int defenseValue;
	/**
	 * Mining value of the creature
	 */
	private int miningValue = 1;
	/**
	 * Cause of death of the creature
	 */
	private String causeOfDeath;
	/**
	 * Vision radius of the creature
	 */
	private int visionRadius;
	/**
	 * Inventory of the creature
	 */
	private Inventory inventory;
	/**
	 * Defense equipments of the creature
	 */
	private Item[] equipment;
	/**
	 * prices of the item in the inventory of the creature (merchants only)
	 */
	private int[] prices;
	/**
	 * number of items in the stock (merchants only)
	 */
	private int cptStock;
	/**
	 * regen hp cooldown of the creature
	 */
	private int regenHpCooldown;
	/**
	 * regen hp of the creature
	 */
	private int regenHpPer1000;
	/**
	 * regen mana cooldown of the creature
	 */
	private int regenManaCooldown;
	/**
	 * regen mana of the creature
	 */
	private int regenManaPer1000;
	/**
	 * current xp of the creature
	 */
	private int xp;
	/**
	 * level of the creature
	 */
	private int level;
	/**
	 * list of effects on the creature
	 */
	private List<Effect> effects;
	/**
	 * Indicator of the fact that the player is slipping on ice
	 */
	private boolean slipping = false;
	/**Counter used to tell if the player can move in a water tile or no
	 * 
	 */
	private int cptMove = 0;
	/**
	 * Counts the number of turn during which the player didn't breath
	 */
	private int breathCpt = 0;
	/**
	 * Indicates if the player can see all the creatures on the map or not
	 */
	private int detectCreatures;
	/**
	 * Indicates if the player can see all the tiles of the map or not
	 */
	private boolean visibility;
	/**
	 * Indicates if the creature is alive or not
	 */
	private boolean isAlive = true;
	
	/**
	 * Constructor
	 * @param world
	 * @param glyph
	 * @param color
	 * @param name
	 * @param maxHp
	 * @param attack
	 * @param defense
	 */
	public Creature(World world, char glyph, Color color, String name, int maxHp, int attack, int defense) {
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.maxHp = maxHp;
		this.hp = maxHp;
		this.attackValue = attack;
		this.defenseValue = defense;
		this.visionRadius = 9;
		this.name = name;
		this.inventory = new Inventory(20);
		this.maxFood = 5000;
		this.food = maxFood / 3 * 2;
		this.level = 1;
		this.regenHpPer1000 = 10;
		this.effects = new ArrayList<Effect>();
		this.maxMana = 100;
		this.mana = maxMana;
		this.regenManaPer1000 = 20;
		this.visibility = false;
		this.miningValue = 1;
		this.equipment = new Item[4];
		this.money = 0;
		this.cptStock = 0;
		this.xp = 0;
	}

	/**
	 * Modify the hp of this
	 * @param amount of live to add/remove
	 * @param causeOfDeath
	 */
	public void modifyMaxHp(int amount, String causeOfDeath) {
		maxHp += amount;
		if (hp > maxHp) {
			modifyHp(maxHp - hp, causeOfDeath);
		}
	}

	/**
	 * Add a number of bonus lifes to the creature
	 * @param value
	 */
	public void addLifes(int value) {
		additionalLives += value;
		if (value < 0)
			doAction("are drained out of energy");
		if (value > 0)
			doAction("are fueled with energy");
	}

	/**
	 * Add or remove a certain amount of money from the creature
	 */
	private void modifyMoney(int amount) {
		if (amount > 0) {
			double coinMultiplicator = 1;
			for (int i = 0; i < equipment.length; i++) {
				coinMultiplicator *= equipment[i] != null ? equipment[i].coinMultiplicator() : 1;
			}
			amount = (int) (coinMultiplicator * amount);
			doAction("gain " + amount + " coins");
		}
		money += amount;
	}
	
	/**
	 * Decrease the duration of the equipment in parameter
	 * @param equipment
	 */
	private void equipmentDecay(Item equipment) {
		if (equipment != null) {
			equipment.decreaseDurationValue();
			if (equipment.isBroken() && isPlayer()) {
				notify("Your " + equipment.name() + " broke");
				getRidOf(equipment);
			}
		}
	}

	/**
	 * Return the mining value of the creature pickaxe or not included
	 * @return
	 */
	public int miningValue() {
		return miningValue + ((pickaxe() != null) ? pickaxe().miningValue() : 0);
	};

	/**
	 * Return the attack value of the creature equipment or not included
	 * @return
	 */
	public int attackValue() {
		return attackValue + (weapon() == null ? 0 : weapon().attackValue())
				+ (armor() == null ? 0 : armor().attackValue());
	}

	/**
	 * Return the defense value of the creature equipment or not included
	 * @return
	 */
	public int defenseValue() {
		return defenseValue + (weapon() == null ? 0 : weapon().defenseValue())
				+ (armor() == null ? 0 : armor().defenseValue());
	}
	
	/**
	 * return the creature's xp
	 * @return
	 */
	public int xp() {
		return xp;
	}
	
	/**
	 * sets the creature's xp to -1 which means that the creature was revived and can't give any more xp or money
	 */
	public void noXp() {
		xp = -1;
	}

	/**
	 * indicates if this creature gives money and xp upon killing
	 * @return
	 */
	public boolean givesXp() {
		return xp != -1;
	}
	
	/**
	 * modify the amount of xp and can increase the level of the creature
	 * @param amount
	 */
	public void modifyXp(int amount) {
		xp += amount;

		notify("You %s %d xp.", amount < 0 ? "lose" : "gain", amount);
		while (xp > (int) (Math.pow(level, 1.75) * 25)) {
			level++;
			doAction("advance to level %d", level);
			ai.onGainLevel();
			modifyHp(level * 2, "Died from having a negative level?");
		}
	}

	/**
	 * the creature's level
	 * @return
	 */
	public int level() {
		return level;
	}
	/**
	 * modify the creature's hp regen
	 * @param amount
	 */
	public void modifyRegenHpPer1000(int amount) {
		regenHpPer1000 += amount;
	}

	/**
	 * the list of effects on the creature
	 * @return
	 */
	public List<Effect> effects() {
		return effects;
	}

	/**
	 * this creature's max mana
	 * @return
	 */
	public int maxMana() {
		return maxMana;
	}

	/**
	 * modify the maxmana of this creature
	 * @param amount
	 */
	public void modifyMaxMana(int amount) {
		maxMana += amount;
	}

	/**
	 * the current mana of this creature
	 * @return
	 */
	public int mana() {
		return mana;
	}

	/**
	 * modify the current mana amount of the creature
	 * @param amount
	 */
	public void modifyMana(int amount) {
		mana = Math.max(0, Math.min(mana + amount, maxMana));
	}

	/**
	 * modify the mana regen
	 * @param amount
	 */
	public void modifyRegenManaPer1000(int amount) {
		regenManaPer1000 += amount;
	}

	/**
	 * return the cause of death of the creature
	 * @return
	 */
	public String causeOfDeath() {
		return causeOfDeath;
	}
	
	/**
	 * the number of items to sale
	 * @return
	 */
	public int cptStock() {
		return cptStock;
	}

	
	/**
	 * Instanciate the stock room (merchant only)
	 * @param n
	 */
	public void setStockRoom(int n) {
		this.inventory = new Inventory(n);
		this.prices = new int[n];
	}

	/**
	 * return the item at the index i in the stock
	 * @param i
	 * @return
	 */
	public Item stock(int i) {
		if (i >= cptStock)
			return null;
		return inventory.getItems()[i];
	}

	/**
	 * return the price of the item at the index i in the stock
	 * @param i
	 * @return
	 */
	public int price(int i) {
		if (i >= cptStock)
			return -1;
		return prices[i];
	}

	/**
	 * add a new item with a set price to the stock
	 * @param item
	 * @param price
	 */
	public void addItemToStock(Item item, int price) {
		if (cptStock == inventory.getItems().length)
			return;
		item.setLocation(this);
		prices[cptStock] = price;
		inventory.add(item);
		cptStock++;
	}

	/**
	 * tells if this creature(merchant) can interact with the creature creature.
	 * @param creature
	 * @return
	 */
	public boolean canInteractWith(Creature creature) {
		return ai.target() != creature;
	}
	
	/**
	 * return a key in the inventory
	 * @return
	 */
	public Item key(){
		return inventory.get("key");
	}

	/**
	 * Makes the creature move in the world and interact with the environnement
	 * @param mx
	 * @param my
	 * @param mz
	 */
	public void moveBy(int mx, int my, int mz) {

		Tile actualTile = world.tile(x, y, z);
		if (breathCpt == 10){
			modifyHp(-maxHp, "Death by suffocation.");
			return;
		}
		if(isPlayer()) 
			if (actualTile == Tile.WATER) {
				cptMove = (cptMove + 1) % 2;
				breathCpt++;
				if (cptMove == 1)
					return;
				color = AsciiPanel.blue;
			}else {
				breathCpt = 0;
				color = AsciiPanel.brightWhite;
				if (actualTile == Tile.POISON) 
					this.addEffect(Effect.POISON);
			}
		if (actualTile == Tile.SWAMP) 
			if (isPlayer())
				if (Math.random() < 0.7) {
					doAction("get trapped");
					return;
				}
			else
				if(Math.random() < 0.2) {
					doAction("get trapped");
					return;
				}
		
		if (mx == 0 && my == 0 && mz == 0)
			return;

		Tile tile = world.tile(x + mx, y + my, z + mz);

		if (mz == -1) {
			if (tile == Tile.STAIRS_DOWN) 
				doAction("walk up the stairs to level %d", z + mz + 1);
			else {
				doAction("try to go up but are stopped by the cave ceiling");
				return;
			}
		} else if (mz == 1) {
			if (tile == Tile.STAIRS_UP) {
				doAction("walk down the stairs to level %d", z + mz + 1);
			} else {
				doAction("try to go down but are stopped by the cave floor");
				return;
			}
		}

		Creature other = world.creature(x + mx, y + my, z + mz);

		modifyFood(-1);
		if (other == null) {
			ai.onEnter(x + mx, y + my, z + mz, tile);
			if (!isBat() && (tile == Tile.ICE || (tile == Tile.FLOOR && world.tile(x, y, z) == Tile.ICE))) {
				if (!slipping) {
					doAction("slip");
					slipping = true;
				}
				moveBy(mx, my, mz);
			}
			slipping = false;
		} else
			meleeAttack(other);
	}

	/**
	 * attack the creature other at close range
	 * @param other
	 */
	public void meleeAttack(Creature other) {
		commonAttack(other, attackValue(), "attack the %s for %d damage", other.name);
	}

	/**
	 *  attack the creature other with a ranged weapon
	 * @param other
	 */
	public void rangedWeaponAttack(Creature other) {
		commonAttack(other, attackValue() / 2 + weapon().rangedAttackValue(), "fire a %s at the %s for %d damage",
				weapon().name(), other.name);
	}

	/**
	 * Basic attack on other. If this creature is a cannibalistic zombie and the other is a zombie and dies, this zombie gets a stats boost
	 * @param other
	 * @param attack
	 * @param action
	 * @param params
	 */
	private void commonAttack(Creature other, int attack, String action, Object... params) {
		modifyFood(-2);

		int amount = Math.max(0, attack - other.defenseValue());
		amount = (int) (Math.random() * amount) + 1;
		Object[] params2 = new Object[params.length + 1];
		
		for (int i = 0; i < params.length; i++)
			params2[i] = params[i];
		params2[params2.length - 1] = amount;

		doAction(action, params2);
		other.notifyAttack(this);
		equipmentDecay(weapon());
		equipmentDecay(other.armor());
		
		other.modifyHp(-amount, "Killed by a " + name);
		if (other.hp < 1) {
			if (isCannibalisticZombie() && other.isZombie() && other.givesXp() == true) {
				attackValue = Math.min(attackValue + other.attackValue() / 2 + attackValue(), 200);
				defenseValue = Math.min(defenseValue + other.defenseValue() / 2 + defenseValue(), 100);
				maxHp = Math.min(maxHp + other.maxHp(), 200);
				modifyHp(maxHp(), "Killed by healing");
			}
			if(other.givesXp() == true) {
				gainXp(other);
				gainMoney(other);
			}
		}
	}
	
	/**
	 * the last attacker of this creature
	 * @return
	 */
	public Creature attacker() {
		return ai.attacker();
	}
	
	/**
	 * notify creature that this creature attacked it
	 * @param creature
	 */
	private void notifyAttack(Creature creature) {
		ai.attackedBy(creature);
	}

	/**
	 * gain money from other. if this creature is a familiar and is tamed, the money goest to its master
	 * @param other
	 */
	private void gainMoney(Creature other) {
		if (isTamed()) 
			master().modifyMoney(other.money);
		else
			modifyMoney(other.money);
	}

	/**
	 * gain xp from other. if this creature is a familiar and is tamed, this familiar gain the xp and its master half of the same amount
	 * @param other
	 */
	public void gainXp(Creature other) {
		int amount = other.maxHp + other.attackValue() + other.defenseValue() - level;

		if (amount > 0)
			if (isTamed()) {
				master().modifyXp(amount / 2);
				modifyXp(amount);
			}
			else
				modifyXp(amount);
	}

	/**
	 * modify the amount of hp of this creature. if this creature is an untamed familiar and was defeated by the player, doesn't die and become a pet 
	 * if the player dies and has additional lives, lose an additional life and gets healed;
	 * @param amount
	 * @param causeOfDeath
	 */
	public void modifyHp(int amount, String causeOfDeath) {
		hp += amount;
		this.causeOfDeath = causeOfDeath;
		if (hp > maxHp) {
			hp = maxHp;
		} else if (hp < 1) {
			if (isTameable() && attacker()!= null && attacker().isPlayer()) {
				hp = maxHp;
				attacker().doAction("tame the animal");
				ai.tamedBy(attacker());
				name = "player's pet " + name.split(" ")[1];
			}else if (additionalLives > 0) {
				additionalLives--;
				maxHp = maxHp / 2;
				if (maxHp <= 0)
					maxHp = 1;
				hp = maxHp;
				doAction("get back up");
			} else {
				doAction("die");
				leaveCorpse();
				dies();
				world.remove(this);
			}
		}
	}
	
	/**
	 * Leave a corpse on the ground at the position of this creature or close
	 */
	private void leaveCorpse() {
		Item corpse = new Item('%', color, name + " corpse");
		corpse.modifyFoodValue(5);
		corpse.setDurationValue(10);
		world.addAtEmptySpace(corpse, x, y, z);
		for (Item item : inventory.getItems()) {
			if (item != null)
				drop(item);
		}
	}

	/**
	 * If has a key, use it to open the chest : gains some money from it
	 * @param wx
	 * @param wy
	 * @param wz
	 */
	public void openChest(int wx, int wy, int wz) {
		Item key = key();
		if (key != null) {
			doAction("open the chest");
			int amount = (int)(Math.random() * 100 * (z + 1));
			modifyMoney(amount);
			world.openChest(wx, wy, wz, this);
			getRidOf(key);
		}else
			doAction("need a key");
	}
	
	/**
	 * Mines the wall at (x,y,z)
	 * @param wx
	 * @param wy
	 * @param wz
	 */
	public void mine(int wx, int wy, int wz) {
		modifyFood(-10);
		world.mine(wx, wy, wz, miningValue());
		doAction("mine");
		equipmentDecay(pickaxe());
	}

	/**
	 * Mines the ground if the creature has a shovel and if there is empty ground below
	 * @param wx
	 * @param wy
	 * @param wz
	 */
	public void dig(int wx, int wy, int wz) {
		modifyFood(-50);
		if (shovel() == null) {
			doAction("don't have a shovel");
			return;
		}
		if (!world.tile(x, y, z).isDiggable()) {
			doAction("can't dig here");
			return;
		}
		if (z == world.depth() - 1) {
			doAction("can't go deeper");
			return;
		}
		if (world.tile(x, y, z + 1).isWall() || world.tile(x, y, z + 1) == Tile.STAIRS_DOWN
				|| world.creature(x, y, z + 1) != null || world.item(x, y, z + 1) != null) {
			notify("Something is blocking the way");
			return;
		}
		doAction("dig");
		equipmentDecay(shovel());
		world.dig(wx, wy, wz);
	}

	
	/**
	 * If this creature is a merchant, sells the item at the index itemIndex in the stock to creature
	 * @param creature
	 * @param itemIndex
	 */
	public void sellItem(Creature creature, int itemIndex) {
		ai.sellItem(creature, itemIndex);
	}

	/**
	 * UPdates the state of the creature in a turn;
	 */
	public void update() {
		if (!isAlive())
			return;
		modifyFood(-1);
		regenerateHealth();
		regenerateMana();
		updateEffects();
		ai.onUpdate();
	}

	/**
	 * Decreases the duration of the effects and removes the effects which have done their time 
	 */
	private void updateEffects() {
		List<Effect> done = new ArrayList<Effect>();
		for (Effect effect : effects) {
			//if (isPlayer()) System.out.print(effect.name());
			effect.update(this);
			if (effect.isDone()) {
				effect.end(this);
				done.add(effect);
			}
		}
		effects.removeAll(done);
	}

	/**
	 * Regenerates hp if cooldown is over
	 */
	private void regenerateHealth() {
		regenHpCooldown -= regenHpPer1000;
		if (regenHpCooldown < 0) {
			if (hp < maxHp) {
				modifyHp(1, "Died from regenerating health?");
				modifyFood(-1);
			}
			regenHpCooldown += 1000;
		}
	}

	/**
	 * Regenerates mana if cooldown is over
	 */
	private void regenerateMana() {
		regenManaCooldown -= regenManaPer1000;
		if (regenManaCooldown < 0) {
			if (mana < maxMana) {
				modifyMana(1);
				modifyFood(-1);
			}
			regenManaCooldown += 1000;
		}
	}

	/**
	 * Checks if the creature can move (wx, wy, wz)
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public boolean canEnter(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz).isGround() && world.creature(wx, wy, wz) == null;
	}

	/**
	 * Sends a message to be displayed in playscreen
	 * @param message
	 * @param params
	 */
	public void notify(String message, Object... params) {
		ai.onNotify(String.format(message, params));
	}

	/**
	 * Sends a message of action to the creatures in the vision radius
	 * @param message
	 * @param params
	 */
	public void doAction(String message, Object... params) {
		for (Creature other : getCreaturesWhoSeeMe()) {
			if (other == this) {
				other.notify("You " + message + ".", params);
			} else {
				other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
			}
		}
	}


	/**
	 * List of the creatures in my vision radius 
	 * @return
	 */
	private List<Creature> getCreaturesWhoSeeMe() {
		List<Creature> others = new ArrayList<Creature>();
		int r = 9;
		for (int ox = -r; ox < r + 1; ox++) {
			for (int oy = -r; oy < r + 1; oy++) {
				if (ox * ox + oy * oy > r * r)
					continue;
				Creature other = world.creature(x + ox, y + oy, z);
				if (other == null)
					continue;

				others.add(other);
			}
		}
		return others;
	}

	/**
	 * Changes a verb at third person to second person (if it is not an irregular verb)
	 * @param text
	 * @return
	 */
	private String makeSecondPerson(String text) {
		String[] words = text.split(" ");
		words[0] = words[0] + "s";

		StringBuilder builder = new StringBuilder();
		for (String word : words) {
			builder.append(" ");
			builder.append(word);
		}

		return builder.toString().trim();
	}

	/**
	 * Check if this creature can see a creature at (wx, wy, wz)
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public boolean canSee(int wx, int wy, int wz) {
		return (detectCreatures > 0 && world.creature(wx, wy, wz) != null || ai.canSee(wx, wy, wz));
	}
	
	/**
	 * Dies and notify it
	 */
	private void dies() {
		isAlive = false;
	}
	
	/**
	 * Check if is alive
	 * @return
	 */
	public boolean isAlive(){
		return isAlive;
	}
	
	/**
	 * Return the tile in the world at (wx, wy, wz)
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public Tile realTile(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz);
	}

	/**
	 * Return the tile in the world at (wx, wy, wz) as last seen
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public Tile tile(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.tile(wx, wy, wz);
		else
			return ai.rememberedTile(wx, wy, wz);
	}

	/**
	 * Return the creature other at (wx, wy, wz) if this creature can see the other creatyre
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public Creature creature(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.creature(wx, wy, wz);
		else
			return null;
	}

	/**
	 * Pickup the item in the ground at the same location of the creature if there is something if there is something on the ground
	 */
	public void pickup() {
		Item item = world.item(x, y, z);
		
		if (inventory.isFull() || item == null) {
			doAction("grab at the ground");
		} else {
			doAction("pickup a %s", item.name());
			world.remove(x, y, z);
			inventory.add(item);
			item.setLocation(this);
		}
	}

	/**
	 * Puts the item on the ground at the same location of this creature if possible or close
	 * @param item
	 * @return
	 */
	public Item drop(Item item) {
		if (world.addAtEmptySpace(item, x, y, z)) {
			doAction("drop a " + item.name());
			inventory.remove(item);
			unequip(item);
			return item;
		} else {
			notify("There's nowhere to drop the %s.", item.name());
			return null;
		}
	}

	/**
	 * Modify hunger, if food < 1, dies of hunger
	 * @param amount
	 */
	public void modifyFood(int amount) {
		food += amount;
		if (food > maxFood) {
			maxFood = (maxFood + food) / 2;
			food = maxFood;
			notify("You can't belive your stomach can hold that much!");
			modifyHp(-1, "Killed by overeating.");
		} else if (food < 1 && isPlayer()) {
			modifyHp(-maxHp, "Starved to death.");
		}
	}

	/**
	 * Check if the creature is a player
	 * @return
	 */
	public boolean isPlayer() {
		return glyph == '@';
	}

	/**
	 * Check if the creature is a merchant
	 * @return
	 */
	public boolean isMerchant() {
		return name.contains("merchant");
	}

	/**
	 * Check if the creature is a zombie
	 * @return
	 */
	public boolean isZombie() {
		return name.contains("zombie");
	}
	
	/**
	 * Check if the creature is a cannibalistic zombie
	 * @return
	 */
	public boolean isCannibalisticZombie() {
		return isZombie() && name.contains("cannibalistic");
	}
	
	/**
	 * Check if the creature can be interracted with
	 * @return
	 */
	public boolean isPNJ() {
		return isMerchant() || isTamed();
	}

	/**
	 * Check if the creature is a bat
	 * @return
	 */
	public boolean isBat() {
		return name.contains("bat");
	}
	
	/**
	 * chexks if the creature can be tamed
	 * @return
	 */
	public boolean isTameable() {
		return name.contains("wild");
	}
	
	/**
	 * chexks if the creature is tamed
	 * @return
	 */
	public boolean isTamed() {
		return ai.master() != null;
	}
	
	/**
	 * @return the master of the creature
	 */
	private Creature master(){
		return ai.master();
	}
	
	/**
	 * eat an item
	 * @param item
	 */
	public void eat(Item item) {
		doAction("eat a " + item.name());
		consume(item);
	}

	/**
	 * drinks an item
	 * @param item
	 */
	public void quaff(Item item) {
		doAction("quaff a " + item.name());
		consume(item);
	}

	/**
	 * Consume an item, apply its effect and foodvalue and removes it from the inventory
	 * @param item
	 */
	private void consume(Item item) {
		if (item.foodValue() < 0)
			notify("Gross!");
		modifyFood((int)(item.foodValue() / 100.0 * maxHp));
		getRidOf(item);
	}

	/**
	 * Add an effect to the list of effects if it is not already in it
	 * @param effect
	 */
	private void addEffect(Effect effect) {
		if (effect == null)
			return;
		for (Effect e : effects) 
			if (e.name().contains(effect.name())) {
				System.out.println("already");
				notify("Nothing happened");
				return;
			}
		effect.start(this);
		effects.add(effect);
	}
	
	/**
	 * remove an item from the inventory
	 * @param item
	 */
	private void getRidOf(Item item) {
		inventory.remove(item);
		unequip(item);
	}

	/**
	 * Unequip an item
	 * @param item
	 */
	public void unequip(Item item) {
		if (item == null)
			return;
		if (item == pickaxe()) {
			if (hp > 0)
				doAction("remove a " + item.name());
			setPickaxe(null);
		}
		if (item == shovel()) {
			if (hp > 0)
				doAction("remove a " + item.name());
			setShovel(null);
		}
		if (item == armor()) {
			if (hp > 0)
				doAction("remove a " + item.name());
			setArmor(null);
		} else if (item == weapon()) {
			if (hp > 0)
				doAction("put away a " + item.name());
			setWeapon(null);
		}
		applyMaxHpBonus(-item.maxHpBonus(), "Killed by " + item.name());
	}

	/**
	 * Equip an item
	 * @param item
	 */
	public void equip(Item item) {
		if (!inventory.contains(item)) {
			if (inventory.isFull()) {
				notify("Can't equip %s since you're holding too much stuff.", item.name());
				return;
			} else {
				world.remove(item);
				inventory.add(item);
			}
		}
		if (item.isPickaxe()) {
			unequip(pickaxe());
			doAction("carry a " + item.name());
			setPickaxe(item);
		}
		if (item.isShovel()) {
			unequip(shovel());
			doAction("carry a " + item.name());
			setShovel(item);
		}
		if (!item.isWeapon() && !item.isArmor())
			return;
		if (item.attackValue() + item.rangedAttackValue() >= item.defenseValue()) {
			unequip(weapon());
			doAction("wield a " + item.name());
			setWeapon(item);
		} else {
			unequip(armor());
			doAction("put on a " + item.name());
			setArmor(item);
		}
		applyMaxHpBonus(item.maxHpBonus(), "Killed by " + item.name());
		addLifes(item.additionalLives());
		item.setAdditionalLives(0);

	}

	/**
	 * Applay a maxHpBonus from a relic
	 * @param value
	 * @param CauseOfDeath
	 */
	private void applyMaxHpBonus(int value, String CauseOfDeath) {
		modifyMaxHp(value, CauseOfDeath);
		if (value > 0) {
			doAction("look healthier");
		}
		if (value < 0) {
			if (hp > 0)
				doAction("look less healthy");
		}
	}

	/**
	 * If can see the case return the item at (wx, wy, wz)
	 * @param wx
	 * @param wy
	 * @param wz
	 * @return
	 */
	public Item item(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.item(wx, wy, wz);
		else
			return null;
	}

	/**
	 * The stats of the character
	 * @return
	 */
	public String details() {
		return String.format("  level:%d  attack:%d  defense:%d  hp:%d", level, attackValue(), defenseValue(), hp);
	}

	/**Summon bats in its surroundings
	 * @param x
	 * @param y
	 * @param z
	 */
	public void summonBat(int x, int y, int z) {
		Creature bat = StuffFactory.newSummonedBat();
		bat.setWorld(world);
		if (!bat.canEnter(x, y, z)) {
			return;
		}
		bat.setLocation(x, y, z);
		world.add(bat);
	}


	/**
	 * Modidy the radius of dectection of the creatures
	 * @param amount
	 */
	public void modifyDetectCreatures(int amount) {
		detectCreatures += amount;
	}

	/**
	 * cast a spell at the case (x2, y2, this.Z)
	 * @param spell
	 * @param x2
	 * @param y2
	 */
	public void castSpell(Spell spell, int x2, int y2) {
		Creature other = creature(x2, y2, z);
		if (spell.manaCost() > mana) {
			doAction("point and mumble but nothing happens");
			return;
		} else if (other == null) {
			doAction("point and mumble at nothing");
			return;
		}
		other.addEffect(spell.effect());
		modifyMana(-spell.manaCost());
		System.out.println(mana);
	}

	/**
	 * Check if the creature has the vision of the whole level
	 * @return
	 */
	public boolean hasVisibility() {
		return visibility;
	}

	/**
	 * Gives the vision of the level to the creature
	 */
	public void addVisibility() {
		this.visibility = true;
	}

	/**
	 * Strips the vision of the level from the character
	 */
	public void removevisibility() {
		this.visibility = false;
	}


	@Override
	public Location location() {
		return this;
	}

	@Override
	public void setLocation(Location location) {
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
	}

	@Override
	public void setLocation(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getZ() {
		return z;
	}

	/**
	 * Change the color of the symbol
	 * @param color
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Set the hp to a certain value
	 * @param hp
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}
	
	/**
	 * Set the world to a certain world
	 * @param world
	 */
	public void setWorld(World world) {
		this.world = world;
	}

	/**
	 * The symbol of the creature
	 * @return
	 */
	public char glyph() {
		return glyph;
	}

	/**
	 * Links ai to a certain behahiour
	 * @param ai
	 */
	public void setCreatureAi(CreatureAi ai) {
		this.ai = ai;
	}

	/**
	 * The color of the symbol
	 * @return
	 */
	public Color color() {
		return color;
	}

	/**
	 * The money the creature have
	 * @return
	 */
	public int money() {
		return money;
	}
	
	/**
	 * MaxHP
	 * @return
	 */
	public int maxHp() {
		return maxHp;
	}

	/**
	 * current hp
	 * @return
	 */
	public int hp() {
		return hp;
	}
	
	/**
	 * AdditionalLives left
	 * @return
	 */
	public int additionalLives() {
		return additionalLives;
	}
	
	/**
	 * Set the money with a certain amount
	 * @param amount
	 */
	public void setMoney(int amount) {
		money += amount;
	}

	/**
	 * Set the attack value to a certain value
	 * @param value
	 */
	public void modifyAttackValue(int value) {
		attackValue += value;
	}
	
	/**
	 * Set the defense value to a certain value
	 * @param value
	 */
	public void modifyDefenseValue(int value) {
		defenseValue += value;
	}
	
	/**
	 * Set the vision radius to a certain value
	 * @param value
	 */
	public void modifyVisionRadius(int value) {
		visionRadius += value;
	}

	/**
	 * vision radius
	 * @return
	 */
	public int visionRadius() {
		return visionRadius;
	}

	/**
	 * name of the creature
	 * @return
	 */
	public String name() {
		return name;
	}

	/**
	 * inventory of the creature
	 * @return
	 */
	public Inventory inventory() {
		return inventory;
	}

	/**
	 * maxfood of the creature
	 * @return
	 */
	public int maxFood() {
		return maxFood;
	}

	/**
	 * hunger of the creature
	 * @return
	 */
	public int food() {
		return food;
	}

	/**
	 * Weapon of the creature
	 * @return
	 */
	public Item weapon() {
		return equipment[0];
	}

	/**
	 * sets the weapon of the creature with a certain item
	 * @param item
	 */
	public void setWeapon(Item item) {
		equipment[0] = item;
	}

	/**
	 * armor of the creature
	 * @return
	 */
	public Item armor() {
		return equipment[1];
	}

	/**
	 * sets the armor of the creature with a certain item
	 * @param item
	 */
	public void setArmor(Item item) {
		equipment[1] = item;
	}

	/**
	 * pickaxe of the creature
	 * @return
	 */
	public Item pickaxe() {
		return equipment[2];
	}

	/**
	 * sets the pickaxe of the creature with a certain item
	 * @param item
	 */
	public void setPickaxe(Item item) {
		equipment[2] = item;
	}

	/**
	 * shovel of the creature
	 * @return
	 */
	public Item shovel() {
		return equipment[3];
	}
	
	/**
	 * sets the shovel of the creature with a certain item
	 * @param item
	 */
	public void setShovel(Item item) {
		equipment[3] = item;
	}

	
}