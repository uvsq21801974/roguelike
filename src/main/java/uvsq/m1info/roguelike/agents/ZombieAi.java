package uvsq.m1info.roguelike.agents;

import java.util.List;

import uvsq.m1info.roguelike.map.Point;


public class ZombieAi extends CreatureAi {
	private static final long serialVersionUID = -8338520814060653899L;
	
	/**
	 * target of the zombie
	 */
	protected Creature target;
	/**Constructor of a zombie that wander when he can't see its target and that hunt its target otherwise
	 * @param creature
	 * @param player
	 */
	public ZombieAi(Creature creature, Creature player) {
		super(creature);
		this.target = player;
	}

	@Override
	public void onUpdate(){
		if (Math.random() < 0.2)
			return;
		if (creature.canSee(target.x, target.y, target.z))
			hunt(target);
		else
			wander();
	}
	
	
	@Override
	public void wander(){
    	List<Point> n = Point.neighborsN(1);
    	for (Point neighbor : n){
            Creature other = creature.creature(creature.x + neighbor.x, creature.y + neighbor.y, creature.y);
    		if(other == null || !other.name().contains("zombie")) {
    			creature.moveBy(neighbor.x, neighbor.y, 0);
    			return;
    		}
    	}
	}
}
