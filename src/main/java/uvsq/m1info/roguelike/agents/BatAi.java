package uvsq.m1info.roguelike.agents;

public class BatAi extends CreatureAi {
	private static final long serialVersionUID = 279848692791455604L;

	/**
	 * behaviour of a bat
	 * @param creature
	 */
	public BatAi(Creature creature) {
		super(creature);
	}

	public void onUpdate(){
		wander();
		wander();
	}
}
