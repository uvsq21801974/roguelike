package uvsq.m1info.roguelike;

import java.io.Serializable;

public class Spell implements Serializable {
	private static final long serialVersionUID = 220150248471177591L;
	/**
	 * name of the spell
	 */
	private String name;
	public String name() { return name; }

	/**
	 * cost in mana of the spell
	 */
	private int manaCost;
	public int manaCost() { return manaCost; }

	/**
	 * effect of the spell
	 */
	private Effect effect;
	public Effect effect() { 
		return effect; 
	}

	/**
	 * target of the spell
	 */
	private boolean target;
	public boolean requiresTarget() { 
		return target; 
	}
	
	public Spell(String name, int manaCost, Effect effect, boolean target){
		this.name = name;
		this.manaCost = manaCost;
		this.effect = effect;
		this.target = target;
	}
}
