package uvsq.m1info.roguelike.agents;

import java.util.List;

import uvsq.m1info.roguelike.map.Path;
import uvsq.m1info.roguelike.map.Point;

public class FamiliarAi extends PNJAi{
	private static final long serialVersionUID = -717686996195789808L;
	/**
	 * Master of the familiar
	 */
	private Creature master;
	/**
	 * Constructor of a familiar that can be tamed and protect its master
	 * @param creature
	 */
	public FamiliarAi(Creature creature) {
		super(creature);
	}

	@Override
	public void onUpdate(){
		
		if(master == null && target == null) {
			wander();
			return;
		}
		if (master == target) 
			target = null;
		if (master == null) {
			if (target != null && target.isAlive() && creature.canSee(target.x, target.y, target.z)) {
				hunt(target);
			}else {
				target = null;
				wander();
			}
			return;
		}
		if(master.attacker() == null || master.attacker().isTamed() || !master.attacker().isAlive())
			hunt(master);
		else 
			hunt(master.attacker());
	}
	
	@Override
	public Creature attacker(){
		return target;
	}
	
	@Override
	public void tamedBy(Creature creature){
		master = creature;
		target = null;
	}
	
	@Override
	public void wander(){
    	List<Point> n = Point.neighborsN(1);
    	for (Point neighbor : n){
            Creature other = creature.creature(creature.x + neighbor.x, creature.y + neighbor.y, creature.y);
    		if(other == null) {
    			creature.moveBy(neighbor.x, neighbor.y, 0);
    			return;
    		}
    	}
	}
	
	@Override
	public void hunt(Creature target) {
		List<Point> points = new Path(creature, target.x, target.y).points();
		if (points == null || points.size() == 0) {
			creature.moveBy(target.x - creature.x, target.y - creature.y, 0);
			return;
		}
		int mx = points.get(0).x - creature.x;
		int my = points.get(0).y - creature.y;
		if (master == null || creature.creature(creature.x + mx, creature.y + my, creature.z) != master) 
			creature.moveBy(mx, my, 0);
	}
	
	@Override
	public Creature master() {
		return master;
	}
}
