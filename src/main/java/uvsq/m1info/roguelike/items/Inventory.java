package uvsq.m1info.roguelike.items;

import java.io.Serializable;

public class Inventory implements Serializable {

	private static final long serialVersionUID = 5878884524705734032L;
	/**
	 * Array of items in the inventory
	 */
	private Item[] items;
	/**
	 * the items in the inventory
	 * @return
	 */
	public Item[] getItems() { return items; }
	/**
	 * the item at index i in the inventory
	 * @param i
	 * @return
	 */
	public Item get(int i) { return items[i]; }
	
	/**
	 * Instanciate the inventory if a max number of item
	 * @param max
	 */
	public Inventory(int max){
		items = new Item[max];
	}
	
	/**
	 * add an item at an empty space in the inventory
	 * @param item
	 */
	public void add(Item item){
		for (int i = 0; i < items.length; i++){
			if (items[i] == null){
				items[i] = item;
				break;
			}
		}
	}

	/**
	 * remove an item from the inventory
	 * @param item
	 */
	public void remove(Item item){
		for (int i = 0; i < items.length; i++){
			if (items[i] == item){
				items[i] = null;
				return;
			}
		}
	}

	/**
	 * check if there is no more empty space in the inventory
	 * @return
	 */
	public boolean isFull(){
		int size = 0;
		for (int i = 0; i < items.length; i++){
			if (items[i] != null)
				size++;
		}
		return size == items.length;
	}
	
	/**
	 * check if the inventory contains the irem
	 * @param item
	 * @return
	 */
	public boolean contains(Item item) {
		for (Item i : items){
			if (i == item)
				return true;
		}
		return false;
	}
	
	/**
	 * return the first item with the same name
	 * @param name
	 * @return
	 */
	public Item get(String name) {
		for (Item i : items){
			if (i == null)
				continue;
			if (i.name().contains(name))
				return i;
		}
		return null;
	}
}
